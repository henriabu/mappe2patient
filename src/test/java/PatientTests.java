import ntnu.idatt2001.henriabu.mappe2.Patient;
import ntnu.idatt2001.henriabu.mappe2.PatientRegister;
import ntnu.idatt2001.henriabu.mappe2.fileSaving.FileChooserClass;
import org.junit.jupiter.api.*;

import java.io.File;


public class PatientTests {
    Patient patient1;
    Patient patientWithInvalidSSN;
    Patient patientWithSameSSNAsPatient1;
    PatientRegister register;

    @BeforeEach
    public void beforeEach(){
        patient1 = new Patient("Ola", "Normann", "Devold", "12123123121", "Cold");
        patientWithInvalidSSN = new Patient("Elle", "Melle", "Deg", "A9212212345", "Fortelle");
        patientWithSameSSNAsPatient1 = new Patient("Kari", "Svenske", "Naturen", "12123123121", "Forgiftet");
        register = new PatientRegister();

    }
    @Nested
    class patientRegister{
        @Test
        @DisplayName("Adding patient. Should be added")
        public void addPatientShouldAssertTrue(){
            Assertions.assertTrue(register.addPatient(patient1));
        }

        @Test
        @DisplayName("Adding patient. Should not be added. Social Security Number invalid")
        public void addPatientWithInvalidSSNShouldAssertFalse(){
            Assertions.assertFalse(register.addPatient(patientWithInvalidSSN));
        }
        @Test
        @DisplayName("Adding patient with same SSN as existing patient. Should assert false")
        public void addPatientWithIdenticalSSNShouldAssertFalse(){
            register.addPatient(patient1);
            Assertions.assertFalse(register.addPatient(patientWithSameSSNAsPatient1));
        }
    }



}
