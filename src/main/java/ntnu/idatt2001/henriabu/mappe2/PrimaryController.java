package ntnu.idatt2001.henriabu.mappe2;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.List;
import java.util.Optional;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import ntnu.idatt2001.henriabu.mappe2.fileSaving.FileChooserClass;
import ntnu.idatt2001.henriabu.mappe2.fileSaving.FileHandler;

/**
 * The controller that controlls the main window. Uses fxml and therefore doesn't use the
 * GUI factory
 */
public class PrimaryController {

    private @FXML ImageView imageViewAddPerson;
    private @FXML ImageView imageViewRemovePerson;
    private @FXML ImageView imageViewEditPerson;
    private Image addPersonImage = new Image(new FileInputStream("images/AddPerson.png"));
    private Image removePersonImage = new Image(new FileInputStream("images/RemovePerson.png"));
    private Image editPersonImage = new Image(new FileInputStream("images/EditPerson.png"));
    private @FXML TableView<Patient> patientTableView;
    private @FXML TableColumn<Patient, String> fNameTableColumn;
    private @FXML TableColumn<Patient, String> lNameTableColumn;
    private @FXML TableColumn<Patient, String> ssnTableColoumn;

    private PatientRegister patientRegister = new PatientRegister();


    public PrimaryController() throws FileNotFoundException {

    }

    /**
     * When the application is initialized uses help methods to set the cellvalues for the
     * the table columns, set the images in the imageview, make it possible to double click
     * a patient, and set the tableview to the patient register
     */
    public void initialize(){
        setCells();
        setImages();
        doubleClickOnRow();
        patientTableView.setItems(patientRegister.getPatients());
    }

    /**
     * Method for creating and adding patient. Uses PatientDialaog and the create method
     * that is initialized in that constructor.
     */
    public void addNewPatient() {
        //Since there is no argument passed in the constructor to create a patient is used
        PatientDialog patientDialog = new PatientDialog();
        Optional<Patient> patient = patientDialog.showAndWait();

        if (patient.isPresent()){
            //Sets patient equal to the patient that is created in PatientDialog
            //There is no requirement to fill in general practitioner or diagnosis since this isn't
            //essential information to create a patient and can be edited later
            Patient patient1 = patient.get();
            //If any of the first three fields are empty the user is told and the method is run again
            if (patient.get().getFirstName() == null || patient.get().getFirstName().equals("")){
                FactoryGUI.showErrorMessage("Couldn't add patient", "First name is missing");
                addNewPatient();
            }
            else if(patient.get().getLastName() == null || patient.get().getLastName().equals("")){
                FactoryGUI.showErrorMessage("Couldn't add person", "Last name is missing");
                addNewPatient();
            }
            else if(patient.get().getSocialSecurityNumber() == null || patient.get().getSocialSecurityNumber().equals("") || !checkIfSSNIsValid(patient.get().getSocialSecurityNumber())){
                FactoryGUI.showErrorMessage("Couldn't add person", "Social security number must be 11 digits and integers only");
                addNewPatient();
            }


            else{
                //The created patient is added to the list and the tableview is refreshed
                patientRegister.addPatient(patient1);
                patientTableView.refresh();}
        }
    }

    /**
     * Similarly to the add method uses PatientDialog and the edit method there to edit
     * a chosen patient
     */
    public void editPatient(){
        try{
            //Uses the help method selectedPatient set the patient that is to be edited equal
            //to the patient that is selected
            Patient e = seletedPatient();

            //Since patient e is passed in as an argument the constructor that edits a patient is used
            PatientDialog patientDialog = new PatientDialog(e);
            //used the the method in patientdialog to set textfields to set them equal to the patient's information
            patientDialog.setTextFieldsForEditing(e.getFirstName(), e.getLastName(), e.getSocialSecurityNumber(), e.getGeneralPractitioner(), e.getDiagnosis());
            Optional<Patient> patient = patientDialog.showAndWait();

            if (patient.isPresent()){
                //If any of the first three fields are empty the user is told and the method is run again
                //There is no requirement to fill in general practitioner or diagnosis since this isn't
                //essential information to create a patient and can be edited later
                if (patient.get().getFirstName() == null || patient.get().getFirstName().equals("")){
                    FactoryGUI.showErrorMessage("Couldn't edit patient", "First name is missing");
                    editPatient();
                }
                else if(patient.get().getLastName() == null || patient.get().getLastName().equals("")){
                    FactoryGUI.showErrorMessage("Couldn't edit person", "Last name is missing");
                    editPatient();
                }
                else if(patient.get().getSocialSecurityNumber() == null || patient.get().getSocialSecurityNumber().equals("") || !checkIfSSNIsValid(patient.get().getSocialSecurityNumber())){
                    FactoryGUI.showErrorMessage("Couldn't edit person", "Social security number must be 11 digits and integers only");
                    editPatient();
                }
                else{
                    //Patientregister uses it's editPatient function to set the original patient object's
                    // variables equal to the one created in the fuction, and the tableview is refreshed
                    patientRegister.editPatient(e, patient.get().getFirstName(), patient.get().getLastName(),
                            patient.get().getSocialSecurityNumber(), patient.get().getGeneralPractitioner(), patient.get().getDiagnosis());
                    patientTableView.refresh();
                }
            }
        }
        catch (NullPointerException e){
            FactoryGUI.showErrorMessage("Error", "No patient chosen");
        }
    }

    /**
     * Method to remove a selected patient. If no patient is selected an error message
     * is shown
     */
    public void removePatient(){
        try {
            Patient patient = seletedPatient();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Remove patient");
            //Asks for confirmation before removing the chosen patient
            alert.setContentText("Press ok to remove " + patient.getFirstName() + " " + patient.getLastName());
            Optional<ButtonType> result = alert.showAndWait();
            //Patient is removed if a patient is chosen and ok is pressed
            if (result.isPresent() && patient != null && result.get() == ButtonType.OK) {
                patientRegister.removePatient(patient);
            }
        }
        catch (NullPointerException e){
            FactoryGUI.showErrorMessage("Error", "No patient chosen");
        }
    }

    /**
     * The method that shows the information about the program with an alert box
     */
    public void viewAbout(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText("Patient register\nv 1.0-SNAPSHOT");
        alert.setContentText("Application created by Henrik Burmann");
        alert.showAndWait();
    }

    /**
     *Method listens after and handles when user double clicks a patient. Information about the
     * patient is then shown
     */
    public void doubleClickOnRow(){
        //creates a table row i the tableview and listens if the row is clicked twice.
        patientTableView.setRowFactory(tv -> {
            TableRow<Patient> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if(event.getClickCount() == 2 && !(row.isEmpty())){
                    //when a row is clicked twice patient is set equal to the item the row
                    //contains
                    Patient patient = row.getItem();
                    //The information about the patient is shown
                    FactoryGUI.informationAboutPatient(patient);
                }
            });
            return row;
        });
    }

    /**
     * Creates instances of FileChooserClass and FileHandler to write the patient register
     * to the chosen file
     * @throws IOException
     */
    public void writeToFile() throws IOException {
        Stage stage = new Stage();
        FileChooserClass fileChooser = new FileChooserClass();
        FileHandler fileHandler = new FileHandler();
        fileHandler.writeToCsv(fileChooser.csvExport(stage), patientRegister.getPatients());
    }

    /**
     * Creates instances of FileChooser and FileHandler so the user can choose a file and
     * read form it.
     * @throws IOException
     */
    public void readFromFile() throws IOException {
        Stage stage = new Stage();
        FileChooserClass fileChooser = new FileChooserClass();
        try{
            String fileName = fileChooser.csvImport(stage);
            FileHandler fileHandler = new FileHandler();
            //Sets the list importedPatients equal to the list that is created when readCSV in
            //fileHandler is called with a file as an input.
            List<Patient> importedPatients = fileHandler.readCSV(fileName);
            //Clears the register and thereby the tableview before registering the new patients
            clearPatientRegister();
            //Adds all the patients in importedPatients to the patient register
            for (Patient p: importedPatients){
                patientRegister.addPatient(p);
            }
            patientTableView.refresh();
        }
        catch (IllegalArgumentException e){
            //If an invalid file type is chosen the user gets an error message the the method is
            //called again
            FactoryGUI.showErrorMessage("Couldn't load file", "The chosen" +
                    "file type is not valid.\nPlease choose another file or cancel the operation");
            readFromFile();
        }
        catch (NullPointerException e){}
    }

    /**
     * Help method that checks if a patients social security number is valid.
     * A valid social security number is 11 digits long and contains integers only
     * @param ssn The social security number
     * @return True if the ssn is valid. False if it is invalid
     */
    private static boolean checkIfSSNIsValid(String ssn){
        if (ssn.length() == 11 && (ssn.matches("[0-9]+"))){
            return true;
        }

        return false;
    }

    /**
     * Help method that clears the patient register
     */
    private void clearPatientRegister(){
        patientRegister.getPatients().clear();
    }

    /**
     * Help method called in the initialize method, setting the images to their respective
     * imageviews
     */
    private void setImages(){
        imageViewAddPerson.setImage(addPersonImage);
        imageViewRemovePerson.setImage(removePersonImage);
        imageViewEditPerson.setImage(editPersonImage);
    }

    /**
     * Help method called in the initialize method, that sets the cellValue for the table columns
     * Only first name, last name and social security number is shown, since to much
     * information about every patient in an overview is unnecessary.
     * A patients total information is shown if double clicked
     */
    private void setCells(){
        fNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        ssnTableColoumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
    }

    /**
     * Help method to get the patient in the tableview that is clicked
     * @return The selected patient
     */
    private Patient seletedPatient(){
        return patientTableView.getSelectionModel().getSelectedItem();
    }

    /**
     * Exits application
     */
    public void exitApplication(){
        Platform.exit();
    }
}
