package ntnu.idatt2001.henriabu.mappe2;

/**
 * Class representing a patient
 */
public class Patient {
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     *
     * @param socialSecurityNumber
     * @param firstName
     * @param lastName
     * @param generalPractitioner
     * @param diagnosis The example file comes without diagnosis and therefore diagnosis is
     *                  set to "Unspecified" when it is null or empty.
     */
    public Patient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber, String diagnosis){
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.generalPractitioner = generalPractitioner;
        this.diagnosis = diagnosis;
        if (diagnosis == null || diagnosis.equals("")){
            setDiagnosis("Unspecified");
        }
    }

    /**
     *
     * @return socialSecurityNumber
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Setter for socialSecurityNumber
     * @param socialSecurityNumber
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     *
     * @return firstname
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter for firstName
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter for lastName
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Setter for diagnosis
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     *
     * @return generalPractitioner
     */
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * Setter for generalPractitioner
     * @param generalPractitioner
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Equals method for patient. A patient is equall to another patient if
     * the social security numbers are equal, since SSNs are uniqe to a person
     * @param o
     * @return True if the object is of type Patient with the same SSN as the one it is compared to
     * Also returns true if it is the same object.
     */
    public boolean equals(Object o){
        if (this == o){
            return true;
        }
        else if(!(o instanceof Patient)){
            return false;
        }
        Patient patient =(Patient) o;
        return patient.getSocialSecurityNumber().equals(this.socialSecurityNumber);
    }

    /**
     *
     * @return the patient's full name
     */
    public String getFullName(){
        return firstName + " " + lastName;
    }

    /**
     *
     * @return toString with lastName and diagnosis, though it is never used.
     */
    public String toString(){
        return "Name: " + lastName + " Diagnosis: " + diagnosis;
    }
}
