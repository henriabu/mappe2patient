package ntnu.idatt2001.henriabu.mappe2.fileSaving;

import javafx.collections.ObservableList;
import ntnu.idatt2001.henriabu.mappe2.Patient;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * The class responsible for writing to and reading from files
 */
public class FileHandler {
    public FileHandler(){

    }

    /**
     * The method writes an observable list to a specified file
     * @param fileName The string value of the path to the file that is being written to
     * @param patients The list of patient that is being written to the file
     * @throws IOException
     */
    public void writeToCsv(String fileName, ObservableList<Patient> patients) throws IOException {
        FileWriter fileWriter = new FileWriter(fileName);
        // filewriter splits the patients' variables with a semi colon and writes it to a list
        // with all the patient
        for (Patient patient: patients){
            fileWriter.write(patient.getFirstName() +
                    ";" +patient.getLastName() + ";" +
                    patient.getGeneralPractitioner() + ";" + patient.getSocialSecurityNumber() + ";" +
                    patient.getDiagnosis() + "\n");
        }
        fileWriter.close();
    }

    /**
     * The method reads a file and creates patients with the information
     * @param fileName the string value of the path to the string the user wants to read
     * @return A list of the patients in the file
     * @throws IOException
     */
    public List<Patient> readCSV(String fileName) throws IOException {
        List<Patient> patients = new ArrayList<>();
        BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(fileName));

        String line;
        //The while loop will, as long as there is a new line in the file, create a new patient
        // with the given information

        while ((line = bufferedReader.readLine()) != null){
            String [] patientsArray = line.split(";"); //splits the line into substrings at the semicolons

            //Uses the substring to set the patient's information
            patients.add(new Patient(patientsArray[0], patientsArray[1], patientsArray[2],
                    patientsArray[3], patientsArray[4]));
        }
        bufferedReader.close();
        return patients;

    }
}
