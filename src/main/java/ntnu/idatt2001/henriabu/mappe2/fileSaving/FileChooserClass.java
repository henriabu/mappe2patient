package ntnu.idatt2001.henriabu.mappe2.fileSaving;

import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ntnu.idatt2001.henriabu.mappe2.FactoryGUI;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * The class responsible to choose the file to write to / read from.
 */
public class FileChooserClass {

    /**
     * The method the chooses the file to import and checks whether or not the file is valid
     * @param stage The stage the filechooser is set in
     * @return The string value of the path to the file the user want to import from
     * @throws IOException
     */
    public String csvImport(Stage stage) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose file");
        //Sets the initial directory to the PatientFiles directory in resources
        fileChooser.setInitialDirectory(new File("src/main/resources/PatientFiles"));
        //Sets file equal to the chosen file in filechooser and sets it's canonicalPath equal
        //to string path.
        File file = fileChooser.showOpenDialog(stage);
        String path = file.getCanonicalPath();

        //uses the getExtention() method to check if the chosen file is of type csv and therefore valid
        //If not an IllegalArgumentException is thrown
        if (!getExtention(file).equals("csv")){
            throw new IllegalArgumentException("File type must be of CSV");
        }
        return path;
    }

    /**
     * Method that chooses the file that is to be written to
     * @param stage The stage the filechooser is set in
     * @return the file's canonical path
     * @throws IOException
     */
    public String csvExport(Stage stage) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose file to write to");
        //Adds an extensionfilter so that the user i forced to choose a csv file
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV", "*.csv"));
        fileChooser.setInitialDirectory(new File("src/main/resources/PatientFiles"));
        File file = fileChooser.showSaveDialog(stage);
        return file.getCanonicalPath();
    }

    /**
     * Gets a file's extension so csvImport method can check if a file is valid
     * @param file
     * @return the files extension
     */
    private String getExtention(File file){
        String fileName = file.toString();

        // Finds the index of the last dot char in the string
        int index = fileName.lastIndexOf(".");
        String extention = null;
        if (index>0){
            //Set extension equal to the rest of the string after the last dot
            extention = fileName.substring(index + 1);
        }
        return extention;
    }
}

