package ntnu.idatt2001.henriabu.mappe2;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

/**
 * Factory with methods that creates nodes and alerts used in the application
 */
public class FactoryGUI {

    /**
     * Creates nodes that are used some places in the code. Isn't used in the controller
     * because fxml is used there.
     * @param type Type of node that is to be created
     * @return the requested node.
     */
    public static Node getNode(String type){
        if (type.equalsIgnoreCase("Button")){
            return new Button();
        }
        else if (type.equalsIgnoreCase("Textfield")){
            return new TextField();
        }
        else if(type.equalsIgnoreCase("Label")){
            return new Label();
        }
        else if (type.equalsIgnoreCase("Gridpane")){
            return new GridPane();
        }
        else if (type.equalsIgnoreCase("TableView")){
            return new TableView<>();
        }
        else if(type.equalsIgnoreCase("ImageView")){
            return new ImageView();
        }
        else if (type.equalsIgnoreCase("MenuBar")){
            return new MenuBar();
        }
        return null;
    }

    /**
     * Creates the alert box that is used to alert the user about errors
     * @param titleMessage The title of the alert box
     * @param reason The reason the error occurred. Could for example be a textfield that is empty
     */
    public static void showErrorMessage(String titleMessage, String reason){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(titleMessage);
        alert.setContentText(reason);
        alert.showAndWait();
    }

    /**
     * Creates the alert that shows information about a chosen patient
     * @param patient The chosen patient
     */
    public static void informationAboutPatient(Patient patient){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText("Information about " + patient.getFullName());
        alert.setContentText("Name: " + patient.getFullName() + "\n" + "Social security number: " +
                patient.getSocialSecurityNumber() + "\n" + "General Practitioner: " +
                patient.getGeneralPractitioner() + "\n" + "Diagnosis: " + patient.getDiagnosis());
        alert.showAndWait();
    }


}
