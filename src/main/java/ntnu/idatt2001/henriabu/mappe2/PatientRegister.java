package ntnu.idatt2001.henriabu.mappe2;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Class responsible for storing the patients in an observableList
 */
public class PatientRegister {
    private ObservableList<Patient> patients;

    public PatientRegister(){
        this.patients = FXCollections.observableArrayList();
    }

    /**
     * Method to add patient. Adds patient if the patient isn't equal to a patient already in the list.
     * A patient is equal to another if their social security numbers are equal.
     * @param patient
     * @return returns true if the patient is added. Returns false if not.
     */
    public boolean addPatient(Patient patient){
        if (!(checkIfSSNIsValid(patient.getSocialSecurityNumber()))){
            return false;
        }
        for(Patient p: patients){
            if (patient.equals(p)){
                return false;
            }
        }
        patients.add(patient);
        return true;
    }

    /**
     * Removes patient from the list
     * @param patient
     */
    public void removePatient(Patient patient){
        patients.remove(patient);
    }

    /**
     *
     * @return patients
     */
    public ObservableList<Patient> getPatients(){
        return patients;
    }

    /**
     * Method for editing chosen patient. Loops through the list and edits the one which social security
     * number belonging the chosen patient. This secures that the right patient is edited.
     * @param chosenPatient The patient who is to be edited
     * @param firstName
     * @param lastName
     * @param ssn Social security number
     * @param gp General practitioner
     * @param diagnosis
     */
    public void editPatient(Patient chosenPatient, String firstName, String lastName,
                               String ssn, String gp, String diagnosis){
        for (int i = 0; i < patients.size(); i++) {
            if (chosenPatient.getSocialSecurityNumber().equals(patients.get(i).getSocialSecurityNumber())){
                patients.get(i).setFirstName(firstName);
                patients.get(i).setLastName(lastName);
                patients.get(i).setSocialSecurityNumber(ssn);
                patients.get(i).setGeneralPractitioner(gp);
                patients.get(i).setDiagnosis(diagnosis);
            }
        }
    }

    /**
     * Help method that checks if a patients social security number is valid.
     * A valid social security number is 11 digits long and contains integers only
     * @param ssn The social security number
     * @return True if the ssn is valid. False if it is invalid
     */
    private boolean checkIfSSNIsValid(String ssn){
        if (ssn.length() == 11 && (ssn.matches("[0-9]+"))){
            return true;
        }
        return false;
    }

}
