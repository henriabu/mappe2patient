package ntnu.idatt2001.henriabu.mappe2;

import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * Class that creates the window used for adding/editing a patient and creates/edits said patient.
 */
public class PatientDialog extends Dialog<Patient> {
    private TextField firstNameT = (TextField) FactoryGUI.getNode("Textfield");
    private TextField lastNameT = (TextField) FactoryGUI.getNode("Textfield");
    private TextField ssnT = (TextField) FactoryGUI.getNode("Textfield");
    private TextField gpT = (TextField) FactoryGUI.getNode("Textfield");
    private TextField diagnosisT = (TextField) FactoryGUI.getNode("Textfield");
    private Patient existingPatient = null;
    private Patient patient = null;

    /**
     * Constructor used when adding a new patient. Doesn't take a patient as an argument since
     * a new is to be created. Calls method create patient and sets boolean addingNewPatient to true
     * so the program knows to open the dialog with "add"-nodes.
     */
    public PatientDialog() {
        createPatient(true);
    }

    /**
     * Constructor used when editing a patient. Takes the patient that is to be edited as an argument.
     * Calls method createPatient and sets boolean addingNewPatient to false so the program knows
     * to open the dialago with "edit"-nodes
     *
     * @param patient The patient who is to be edited
     */
    public PatientDialog(Patient patient) {
        createPatient(false);
        this.existingPatient = patient;
    }

    /**
     * Creates a dialog window which is used both for adding and editing a patient.
     * The boolean "addingNewPatient" set as a result of chosen constructor, decides which set of
     * names the nodes get.
     * It also decides wheter the textfields' text is used to create a new person or editing.
     *
     * @param addingNewPatient boolean which sets if the dialogPane is set to adding or editing mode
     */
    public void createPatient(boolean addingNewPatient) {
        //Creates a dialog pane with two buttons: OK and CANCEL
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        GridPane grid = (GridPane) FactoryGUI.getNode("Gridpane");
        grid.setHgap(15);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        /**
         * If addingNewPatient is true the dialogPane title is set to "Add patient"
         * and prompt text is set for the textfields.
         */
        if (addingNewPatient) {
            setTitle("Add patient");
            firstNameT.setPromptText("First name");
            lastNameT.setPromptText("Last name");
            ssnT.setPromptText("Social Security Number");
            gpT.setPromptText("General Practitioner");
            diagnosisT.setPromptText("Diagnosis");
        }
        //If addingNewPatient is false the title is set to edit patient,
        // and no prompt text is set.
        else {
            setTitle("Edit patient");
        }

        //The textFields are added to the gridPane and their location is set
        grid.add(firstNameT, 1, 0);
        grid.add(lastNameT, 1, 1);
        grid.add(ssnT, 1, 2);
        grid.add(gpT, 1, 3);
        grid.add(diagnosisT, 1, 4);


        //Labels describing the textFields are added to the gridPane
        grid.add(new Label("First name"), 0, 0);
        grid.add(new Label("Last name"), 0, 1);
        grid.add(new Label("Social Security Number"), 0, 2);
        grid.add(new Label("General Practitioner"), 0, 3);
        grid.add(new Label("Diagnosis"), 0, 4);



        getDialogPane().setContent(grid);

        //If addingNewPatient is true the program reads the textfield to create a patient if
        //the user presses OK.
        if (addingNewPatient) {
            setResultConverter((ButtonType button) -> {
                if (button == ButtonType.OK) {
                    patient = new Patient(firstNameT.getText(), lastNameT.getText(),
                            gpT.getText(), ssnT.getText(), diagnosisT.getText());
                    patient.setDiagnosis(diagnosisT.getText());
                    if (patient.getDiagnosis() == null || patient.getDiagnosis().equals("")){
                        patient.setDiagnosis("Unspecified");
                    }
                }

                return patient;
            });
        }
        //If addingNewPatient is false the program reads the textfield to set the existingPatients
        //variables
        else {
            setResultConverter((ButtonType button) -> {
                if (button == ButtonType.OK) {
                    existingPatient.setFirstName(firstNameT.getText());
                    existingPatient.setLastName(lastNameT.getText());
                    existingPatient.setSocialSecurityNumber(ssnT.getText());
                    existingPatient.setGeneralPractitioner(gpT.getText());
                    existingPatient.setDiagnosis(diagnosisT.getText());
                    if (existingPatient.getDiagnosis().equals("") || existingPatient.getDiagnosis() == null){
                        existingPatient.setDiagnosis("Unspecified");
                    }
                }
                return existingPatient;
            });
        }
    }

    /**
     * Method that is used in PrimaryController to set the textfields' values to the existing
     * patient's. This way the user doesn't need to fill out all the fields again.
     *
     * @param firstName
     * @param lastName
     * @param ssn Social security number
     * @param gp General practitioner
     * @param diagnosis
     */
    public void setTextFieldsForEditing(String firstName, String lastName, String ssn, String gp, String diagnosis) {
        firstNameT.setText(firstName);
        lastNameT.setText(lastName);
        ssnT.setText(ssn);
        gpT.setText(gp);
        diagnosisT.setText(diagnosis);
    }
}
