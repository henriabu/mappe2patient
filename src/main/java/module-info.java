module ntnu.idatt2001.henriabu.mappe2 {
    requires javafx.controls;
    requires javafx.fxml;

    opens ntnu.idatt2001.henriabu.mappe2 to javafx.fxml;
    exports ntnu.idatt2001.henriabu.mappe2;
}